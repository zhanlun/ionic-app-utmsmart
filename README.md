# Integrated Ionic App (Staging)
## Steps for Installation
If not yet clone, clone this repo into local machine by entering these commands in terminal/cmd/bash:  
`cd parent_project_folder`  
`git clone https://gitlab.com/scsp3744/shared/ionic-app-utmsmart.git`  

Then, open the project folder using your IDE/code editor.  

*Tips: In Visual Studio Code, use **Ctrl + `** to open the default terminal.*

To install the required node modules, in terminal, at the project root directory, run:  
`npm i`  

Ignore the warnings/audit fix messages. Trying to fix may introduce bugs.

Start server by entering `ionic serve`  

## Integration from Existing Projects
Referring to the project directory structure, each module can have its own folder under **./src/app/**  

![project structure](https://i.imgur.com/i5EqyFD.png)

Of which **landing/** is for the home landing page containing the links for all the modules.

To insert your group's module into this project repo:
1. Check your module's directory, gather necessary codes (including each page's folder, **service(s)** for API requests) into a new folder created for this module. As shown in figure below.  
	![module structure](https://i.imgur.com/VnwMWNV.png)

2. Before pushing commits, **pull** first to ensure no incoming commits, then **solve any possible conflicts**, only then **push** the clean solution.

3. Please do not commit updates on **global shared codes** such as:
- theme/ (currently is in UTM color palette, using color="danger | warning" attributes in HTML)

4. For **app-routing.module.ts**, please follow #2 to prevent conflicts. Same goes with  **landing/ page**.  

5. Fork/merge/branching may be used if want to (instead of pull/push using single repo/branch), but pull/push is simpler.

6. Make sure to not overwrite other groups' modules or shared codes accidentally! Be mindful of which code you are editing/committing.
  
  
## Tips for Visual Studio Code
- Use the search icon on the left sidebar for fast **find & replace** across *all files*. Useful for replacing routing links (  [routerLink]="['/ --> [routerLink]="['**../**  )
- Use the **third** icon on left sidebar for git functions after cloning. You can stage/commit/pull/push using this GUI.
- Install extensions on sidebar if you haven't already.

---
Last updated on 1/7