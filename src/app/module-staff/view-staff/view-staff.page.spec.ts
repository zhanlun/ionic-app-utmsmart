import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ViewStaffPage } from './view-staff.page';

describe('ViewStaffPage', () => {
  let component: ViewStaffPage;
  let fixture: ComponentFixture<ViewStaffPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewStaffPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ViewStaffPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
