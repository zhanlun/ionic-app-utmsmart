import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddContractPageRoutingModule } from './add-contract-routing.module';

import { AddContractPage } from './add-contract.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddContractPageRoutingModule
  ],
  declarations: [AddContractPage]
})
export class AddContractPageModule {}
