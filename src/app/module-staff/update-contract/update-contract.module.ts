import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UpdateContractPageRoutingModule } from './update-contract-routing.module';

import { UpdateContractPage } from './update-contract.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UpdateContractPageRoutingModule
  ],
  declarations: [UpdateContractPage]
})
export class UpdateContractPageModule {}
