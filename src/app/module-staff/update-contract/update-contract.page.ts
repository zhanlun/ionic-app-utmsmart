import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { StaffService } from '../staff.service';
import { NgForm } from '@angular/forms';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-update-contract',
  templateUrl: './update-contract.page.html',
  styleUrls: ['./update-contract.page.scss'],
})
export class UpdateContractPage implements OnInit {
  _id: any;
  staff: any;
  staff_contract: any;

  constructor(
    private route: ActivatedRoute, 
    private router: Router,
    private staffService: StaffService,
    private navCtrl: NavController
  ) {
    this.staff = {};
    this.staff_contract = {};
    this.route.queryParams.subscribe(async params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this._id = this.router.getCurrentNavigation().extras.state._id;

        this.staffService.getSingleContract(this._id).then((staff_contract) => {
          this.staff_contract = staff_contract[0];
          this.staff_contract.date_start = this.staff_contract.date_start.substr(0, 10)
          this.staff_contract.date_end = this.staff_contract.date_end.substr(0, 10)
          this.staff_contract.status = this.staff_contract.status ? 1 : 0;
          
          this.staffService.getSelectedStaff(this.staff_contract.staff_id).then((staff) => {
            this.staff = staff;
          })
        });
        
      }
    });
  }

  ngOnInit() {
  }

  updateContract(form: NgForm) {
    for (const [key, value] of Object.entries(form.value)) {
      if (value === '')
        return;

      this.staff_contract[key] = value;
    }
    this.save();
  }

  save(): void {
    if (this.staff_contract) {
      this.staffService.updateStaffContract(this.staff_contract).then((data) => {
      })
      this.navCtrl.navigateForward('/module-staff/view-staff');
    }
  }
}
