import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UpdateContractPage } from './update-contract.page';

describe('UpdateContractPage', () => {
  let component: UpdateContractPage;
  let fixture: ComponentFixture<UpdateContractPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateContractPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UpdateContractPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
