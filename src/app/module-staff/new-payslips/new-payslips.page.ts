import { Component, OnInit } from '@angular/core';
import { StaffService } from '../staff.service';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { NavController } from '@ionic/angular';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-new-payslips',
  templateUrl: './new-payslips.page.html',
  styleUrls: ['./new-payslips.page.scss'],
})
export class NewPayslipsPage implements OnInit {
  staffs: any;
  

  constructor(
    private route: ActivatedRoute, 
    private router: Router,
    private staffService: StaffService,
    private navCtrl: NavController
  ) {
    this.staffs = [];
    this.staffService.getStaffsWithLatestContract().then((staffs) => {
      this.staffs = staffs;
    })
  }

  ngOnInit() {
  }

  submit(form: NgForm) {
    for (const [key, value] of Object.entries(form.value)) {
      if (value === '')
        return;

      this.staffs[key] = value;
    }
    this.save();
  }

  save(): void {
    if (this.staffs) {
      this.staffService.createPayslips(this.staffs).then((data) => {
      })
      this.navCtrl.navigateForward('/module-staff/home');
    }
  }
}
