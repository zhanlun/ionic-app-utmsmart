import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NewPayslipsPage } from './new-payslips.page';

describe('NewPayslipsPage', () => {
  let component: NewPayslipsPage;
  let fixture: ComponentFixture<NewPayslipsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewPayslipsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NewPayslipsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
