import { Component, OnInit } from '@angular/core';
import { StaffService } from '../staff.service';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-view-contract',
  templateUrl: './view-contract.page.html',
  styleUrls: ['./view-contract.page.scss'],
})
export class ViewContractPage implements OnInit {
  staff_id: any;
  staff: Object;
  staff_contracts: any;

  constructor(
    private route: ActivatedRoute, 
    private router: Router,
    private staffService: StaffService,
    private navCtrl: NavController
  ) {
    this.staff_contracts = [];
    this.staff = {};
    this.route.queryParams.subscribe(async params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.staff_id = this.router.getCurrentNavigation().extras.state.staff_id;

        this.staffService.getContractListOfStaff(this.staff_id).then((staff_contracts) => {
          this.staff_contracts = staff_contracts;
        });

        this.staffService.getSelectedStaff(this.staff_id).then((staff) => {
          this.staff = staff;
        });
      }
    });
  }

  ngOnInit() {
  }

  openUpdatePage(_id) {
    let navigationExtras: NavigationExtras = {
      state: {
        _id: _id
      }
    };
    this.router.navigate(['module-staff/update-contract'], navigationExtras);
  }

  openAddPage(staff_id) {
    let navigationExtras: NavigationExtras = {
      state: {
        staff_id: staff_id
      }
    };
    this.router.navigate(['module-staff/add-contract'], navigationExtras);
  }
}
