import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class StaffService {
  url = 'https://utm-staff-api.herokuapp.com';
  // url = 'http://localhost:7878';

  constructor(private http: HttpClient) { }

  getStaffSimpleList(): Observable<any> {
    const path = '/staff';
    return this.http.get(this.url + path);
  }

  async getSelectInputOptions(): Promise<Object> {
    let results = {};

    forkJoin(
      this.getNationList(),
      this.getLocalStateList(),
      this.getGenderList(),
      this.getReligionList(),
      this.getRaceList(),
      this.getMarriageStatusList(),
      this.getEmployeeStatusList(),
      this.getStaffPositionList(),
      this.getOfficeDeptList()
    ).subscribe(([
      nation_list,
      local_state_list,
      gender_list,
      religion_list,
      race_list,
      marriage_status_list,
      employee_status_list,
      staff_position_list,
      office_dept_list
    ]) => {
      results["nation_list"] = nation_list;
      results["local_state_list"] = local_state_list;
      results["gender_list"] = gender_list;
      results["religion_list"] = religion_list;
      results["race_list"] = race_list;
      results["marriage_status_list"] = marriage_status_list;
      results["employee_status_list"] = employee_status_list;
      results["staff_position_list"] = staff_position_list;
      results["office_dept_list"] = office_dept_list;
    });

    return results;
  }

  createStaff(staff) {
    let response;
    let path = '/staff';
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');

    return this.http.post(
      this.url + path, 
      staff, 
      {headers: headers}
    ).toPromise();
  }

  updateStaff(staff) {
    let response;
    let path = `/staff/${staff._id}`;
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');

    return this.http.put(
      this.url + path, 
      staff, 
      {headers: headers}
    ).toPromise();
  }

  updateStaffContract(contract) {
    let response;
    let path = `/staff_contract_single/${contract._id}`;
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');

    return this.http.put(
      this.url + path, 
      contract, 
      {headers: headers}
    ).toPromise();
  }

  createStaffContract(contract) {
    let response;
    let path = '/staff_contract';
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');

    return this.http.post(
      this.url + path, 
      contract, 
      {headers: headers}
    ).toPromise();
  }

  createPayslips(staffs) {
    let postdata = [];
    for (let staff of staffs) {
      let temp = {
        staff_id: staff._id,
        basic: staff.basic,
        date_issued: new Date(),
        additional_items: null
      };
      
      postdata.push(temp);
    }

    let path = '/payslip';
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');

    return this.http.post(
      this.url + path, 
      postdata, 
      {headers: headers}
    ).toPromise();
  }

  getAllPayslips() {
    const path = '/payslip';
    return this.http.get(this.url + path).toPromise();
  }

  getStaffsWithLatestContract() {
    const path = `/staffs_with_latest_contract/`;
    return this.http.get(this.url + path).toPromise();
  }

  getSelectedStaff(staff_id) {
    const path = `/staff/${staff_id}`;
    return this.http.get(this.url + path).toPromise();
  }

  removeSelectedStaff(staff_id) {
    const path = `/staff/${staff_id}`;
    return this.http.delete(this.url + path).toPromise();
  }

  getContractListOfStaff(staff_id) {
    const path = `/staff_contract/${staff_id}`;
    return this.http.get(this.url + path).toPromise();
  }

  getSingleContract(_id) {
    const path = `/staff_contract_single/${_id}`;
    return this.http.get(this.url + path).toPromise();
  }

  // crazy list
  getNationList(): Observable<any> {
    const path = '/nation';
    return this.http.get(this.url + path);
  }

  getLocalStateList(): Observable<any> {
    const path = '/local_state';
    return this.http.get(this.url + path);
  }

  getGenderList(): Observable<any> {
    const path = '/gender';
    return this.http.get(this.url + path);
  }

  getReligionList(): Observable<any> {
    const path = '/religion';
    return this.http.get(this.url + path);
  }

  getRaceList(): Observable<any> {
    const path = '/race';
    return this.http.get(this.url + path);
  }

  getMarriageStatusList(): Observable<any> {
    const path = '/marriage_status';
    return this.http.get(this.url + path);
  }

  getEmployeeStatusList(): Observable<any> {
    const path = '/employee_status';
    return this.http.get(this.url + path);
  }

  getStaffPositionList(): Observable<any> {
    const path = '/staff_position';
    return this.http.get(this.url + path);
  }

  getOfficeDeptList(): Observable<any> {
    const path = '/office_dept';
    return this.http.get(this.url + path);
  }
}
