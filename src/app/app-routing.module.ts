import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'landing',
    pathMatch: 'full'
  },
  // module exam
  {
    path: 'module-exam/home',
    loadChildren: () => import('./module-exam/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'module-exam',
    redirectTo: 'module-exam/home',
    pathMatch: 'full'
  },
  {
    path: 'module-exam/result-slip',
    loadChildren: () => import('./module-exam/result-slip/result-slip.module').then( m => m.ResultSlipPageModule)
  },
  {
    path: 'module-exam/resultpage/:id',
    loadChildren: () => import('./module-exam/result-page/result-page.module').then( m => m.ResultPagePageModule)
  },
  {
    path: 'module-exam/student-list',
    loadChildren: () => import('./module-exam/student-list/student-list.module').then( m => m.StudentListPageModule)
  },
  {
    path: 'module-exam/update-result',
    loadChildren: () => import('./module-exam/update-result/update-result.module').then( m => m.UpdateResultPageModule)
  },
  {
    path: 'module-exam/lecturer',
    loadChildren: () => import('./module-exam/lecturer/lecturer.module').then( m => m.LecturerPageModule)
  },
  {
    path: 'module-exam/academic-staff',
    loadChildren: () => import('./module-exam/academic-staff/academic-staff.module').then( m => m.AcademicStaffPageModule)
  },
  // module student admission
  {
    path: 'module-student-admission/home',
    loadChildren: () => import('./module-student-admission/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'module-student-admission/message/:id',
    loadChildren: () => import('./module-student-admission/view-message/view-message.module').then( m => m.ViewMessagePageModule)
  },
  {
    path: 'module-student-admission',
    redirectTo: 'module-student-admission/home',
    pathMatch: 'full'
  },
  // module staff
  {
    path: 'module-staff',
    redirectTo: 'module-staff/home',
    pathMatch: 'full'
  },
  {
    path: 'module-staff/home',
    loadChildren: () => import('./module-staff/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'module-staff/view-staff',
    loadChildren: () => import('./module-staff/view-staff/view-staff.module').then( m => m.ViewStaffPageModule)
  },
  {
    path: 'module-staff/add-staff',
    loadChildren: () => import('./module-staff/add-staff/add-staff.module').then( m => m.AddStaffPageModule)
  },
  {
    path: 'module-staff/update-staff',
    loadChildren: () => import('./module-staff/update-staff/update-staff.module').then( m => m.UpdateStaffPageModule)
  },
  {
    path: 'module-staff/view-contract',
    loadChildren: () => import('./module-staff/view-contract/view-contract.module').then( m => m.ViewContractPageModule)
  },
  {
    path: 'module-staff/update-contract',
    loadChildren: () => import('./module-staff/update-contract/update-contract.module').then( m => m.UpdateContractPageModule)
  },
  {
    path: 'module-staff/add-contract',
    loadChildren: () => import('./module-staff/add-contract/add-contract.module').then( m => m.AddContractPageModule)
  },
  {
    path: 'module-staff/new-payslips',
    loadChildren: () => import('./module-staff/new-payslips/new-payslips.module').then( m => m.NewPayslipsPageModule)
  },
  {
    path: 'module-staff/view-payslips',
    loadChildren: () => import('./module-staff/view-payslips/view-payslips.module').then( m => m.ViewPayslipsPageModule)
  },
  {
    path: 'landing',
    loadChildren: () => import('./landing/landing.module').then( m => m.LandingPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
